@extends('layouts.app')
@section('content')
<h1>This is your task list</h1>

@if (Request::is('tasks'))  

<h2><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">All Tasks</a></h2>

@endif

<table class = "table" border ="4" >
<thead>

    <tr>
        <th>Title</th>
        <th>Edit</th>
        <th>Owner</th>
        @cannot('user')  <th>Delete</th>@endcannot
        <th>Status</th>
        @foreach($tasks as $task)
  
    <tr>
    </thead>   
    
    <tr>
    
        <td>{{$task->title}}</td>
        
        <td><a href = "{{route('tasks.edit', $task->id)}}"> edit </a></td>  <!--את החץ נפנה לשם העמודה כמו זאת שנתנו בדאטאבייס -->
        <td>{{$task->user_id}}</td>
        @cannot('user')
        <td><form method = 'post' action = "{{action('TaskController@destroy', $task->id)}}">

        @csrf

        @method ('DELETE')

        <div class = "form-group">
        <input type = "submit" class = "form-control" name = "submit" value = "Delete">
        </div>

        </form></td>@endcannot
        
        
        <td>@if($task->status)
                Done!
                @else 
                @cannot('user')<a href="{{route('tasks.change_status', [ $task ->id, $task ->status ])}}">change status</a></td>@endcannot
                 @endif
                
        </tr>
        
        @endforeach
</table> 


        <a href = "{{route('tasks.create')}}"> Create a new Task</a>




@endsection