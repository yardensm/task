<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $user = User::find($id);//מציג את כל המשימות ליוזר הספציפי
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create tasks..");
        }
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tasks = new Task();
        $id = Auth::id();
        $tasks->title = $request->title;
        $tasks->user_id = $id;
        $tasks->status = 0;
        $tasks->save();
        return redirect('tasks'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tasks = Task::find($id);
        return view('tasks.edit', compact('tasks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tasks = Task::find($id);
        $tasks->update($request->all());
        return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to delete tasks..");
        }
        $tasks = Task::find($id);//מושך את הערך הרלוונטי
        $tasks->delete();
        return redirect('tasks');
    }

    public function change_status($id,Request $request)
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to aprove tasks..");
        }
        $tasks = Task::find($id);
        $tasks->status = 1;
        $tasks->update($request->all());
        return redirect('tasks');

    }

    public function mytasks()
        { 
            $id = Auth::id();
            $user = User::Find($id);
            $tasks = $user->tasks;
            return view('tasks.index', ['tasks' => $tasks]);
        }

}
